# 2.1 Anti-pop board

The purpose of this page is to explain step by step the realization of an anti-pop board for a 2.1 audio amplifier.

When powering ON, This board connects the speakers after 3 seconds. When powering OFF the speakers are disconnected immediately.

The board uses the following components :

 * 2 relays Finder 40.52.9.024.000, 24V 8A.
 * an optocoupler : TIL191
 * an AOP : LM741
 * a schmidt trigger : CD4093
 * some transistors : 2N2222, 2N2907, 2N1711, 2N2219, 2N2905
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/09/amplificateur-classe-d-21.html

